<?php

namespace Drupal\forum_inheritance;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\field\FieldConfigInterface;

class FieldConfiguration {

  /**
   * @var static[]
   */
  protected static $all;

  public static function all() {
    if (!isset(static::$all)) {
      $fieldConfiguraitons = \Drupal::moduleHandler()
        ->invokeAll('forum_inheritance_field_configuration');
      static::$all = [];
      foreach ($fieldConfiguraitons as $fc) {
        static::$all[] = new static($fc['node_bundle'], $fc['term_bundle'], $fc['term_reference'], $fc['query_parameter']);
      }
    }
    return static::$all;
  }

  /**
   * @var string
   */
  protected $nodeBundle;

  /**
   * @var string
   */
  protected $termBundle;

  /**
   * @var string
   */
  protected $termReference;

  /**
   * @var string
   */
  protected $queryParameter;

  /**
   * @var string[]
   */
  protected $fieldMap;

  /**
   * FieldConfiguration constructor.
   *
   * @param string $nodeBundle
   * @param string $termBundle
   * @param string $termReference
   * @param string $queryParameter
   */
  public function __construct(string $nodeBundle, string $termBundle, string $termReference, string $queryParameter) {
    $this->nodeBundle = $nodeBundle;
    $this->termBundle = $termBundle;
    $this->termReference = $termReference;
    $this->queryParameter = $queryParameter;
  }

  /**
   * Get field map of equally named fields.
   *
   * @return array
   */
  public function getFieldMap() {
    if (!isset($this->fieldMap)) {
      /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
      $entityFieldManager = \Drupal::service('entity_field.manager');
      $termFieldDefinitions = $entityFieldManager->getFieldDefinitions('taxonomy_term', $this->getTermBundle());
      $nodeFieldDefinitions = $entityFieldManager->getFieldDefinitions('node', $this->getNodeBundle());
      $commonFields = array_intersect_key($termFieldDefinitions, $nodeFieldDefinitions);
      $commonConfgurableFields = array_filter($commonFields, function (FieldDefinitionInterface $fieldDefinition) {
        // Note this is \Drupal\field\FieldConfigInterface, not
        // \Drupal\Core\Field\FieldConfigInterface.
        return $fieldDefinition instanceof FieldConfigInterface;
      });
      $commonFieldKeys = array_keys($commonConfgurableFields);
      $this->fieldMap = array_combine($commonFieldKeys, $commonFieldKeys);
    }
    return $this->fieldMap;
  }

  /**
   * @return string
   */
  public function getNodeBundle(): string {
    return $this->nodeBundle;
  }

  /**
   * @return string
   */
  public function getTermBundle(): string {
    return $this->termBundle;
  }

  /**
   * @return string
   */
  public function getTermReference(): string {
    return $this->termReference;
  }

  /**
   * @return string
   */
  public function getQueryParameter(): string {
    return $this->queryParameter;
  }

}
