<?php

namespace Drupal\forum_inheritance;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;

class ForumInheritanceHooks {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   */
  public static function hookEntityInsert(EntityInterface $entity) {
    // This moved to ::hookEntityPrepareForm.
  }

  /**
   * Implements hook_entity_update().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public static function hookEntityUpdate(EntityInterface $entity) {
    foreach (FieldConfiguration::all() as $fc) {
      if ($entity instanceof TermInterface && $entity->bundle() === $fc->getTermBundle()) {
        /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
        $entityTypeManager = \Drupal::service('entity_type.manager');

        // Inherit to contained forums.
        $termStorage = $entityTypeManager->getStorage('taxonomy_term');
        $query = $termStorage->getQuery()
          ->condition('parent', $entity->id());
        $containedForumTermIds = $query->execute();
        $containedForumTerms = Term::loadMultiple($containedForumTermIds);
        FieldInheritor::inheritFieldChanges([$entity], $containedForumTerms, $fc->getFieldMap());

        // Inherit to contained forum topics.
        $possibleForumTerms[$entity->id()] = $entity->id();
        $nodeStorage = $entityTypeManager->getStorage('node');
        $query = $nodeStorage->getQuery()
          ->condition($fc->getTermReference(), $possibleForumTerms);
        /** @var \Drupal\taxonomy\TermInterface[] $result */
        $containedForumNodeIds = $query->execute();
        $containedForumNodes = Node::loadMultiple($containedForumNodeIds);
        FieldInheritor::inheritFieldChanges([$entity], $containedForumNodes, $fc->getFieldMap());
      }
    }
  }

  /**
   * Prepopulate foum topic form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $operation
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   *
   * @see forum_form_node_form_alter
   */
  public static function hookEntityPrepareForm(EntityInterface $entity, $operation, FormStateInterface $form_state) {
    foreach (FieldConfiguration::all() as $fc) {
      if ($operation === 'default' && $entity instanceof NodeInterface && $entity->bundle() === $fc->getNodeBundle()
        && $entity->hasField($fc->getTermReference()) && ($forumsField = $entity->get($fc->getTermReference()))
        && $forumsField instanceof EntityReferenceFieldItemListInterface
      ) {
        // We can not rely on the entity values, as forum_form_node_form_alter
        // puts them in later in form_alter. So we simply listen on the same
        // quer parameter. We also duplicate that logic so we are prepared if we
        // want to do it without the currently not too well maintained forum
        // module.
        $forumId = \Drupal::request()->query->get($fc->getQueryParameter());
        if (is_numeric($forumId)) {
          $forumsField->setValue(['target_id' => $forumId]);
        }
        /** @var \Drupal\taxonomy\TermInterface[] $parentForumTerms */
        $parentForumTerms = $forumsField->referencedEntities();
        // Whether we or someone else populated $parentForumTerms, inherit the
        // term fields.
        if ($parentForumTerms) {
          FieldInheritor::copyFieldChanges($parentForumTerms, [$entity], $fc->getFieldMap());
        }
      }
    }
  }

}
