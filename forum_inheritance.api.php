<?php

/**
 * @file
 * Implements hook_forum_inheritance_field_configuration().
 */

/**
 * Implements hook_forum_inheritance_field_configuration().
 */
function hook_forum_inheritance_field_configuration() {
  if (\Drupal::moduleHandler()->moduleExists('forum')) {
    return [
      'forum' => [
        'node_bundle' => 'forum',
        'term_bundle' => 'forums',
        'term_reference' => 'taxonomy_forums',
        'query_parameter' => 'forum_id',
      ],
    ];
  }
}
